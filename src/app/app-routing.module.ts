import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

import {WelcomeComponent} from './welcome/welcome.component';
import {PageNotFoundComponent} from './page-not-found/page-not-found.component';
import {LoginComponent} from './user/login.component';
import {PropertyListComponent} from './property/property-list.component';
import {PropertyDetailsComponent} from './property/property-details.component';
import {PropertyEditComponent} from './property/property-edit.component';
import {FormsModule} from '@angular/forms';

const routes: Routes = [
  {path: 'welcome', component: WelcomeComponent},
  {path: 'login', component: LoginComponent},
  {path: 'api/properties/:id', component: PropertyDetailsComponent},
  {path: 'propertyList', component: PropertyListComponent},
  {path: 'propertyAdd', component: PropertyEditComponent},
  {path: '', pathMatch: 'full', redirectTo: 'welcome'},
  {path: '**', component: PageNotFoundComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes), FormsModule],
  exports: [RouterModule],
})
export class AppRoutingModule {}
