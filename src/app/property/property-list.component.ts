import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {PropertyService} from './property.service';
import {IProperty} from './property';
import {PropertyData} from './property-data';
import 'rxjs/operator/toPromise';

@Component({
  selector: 'app-property-list',
  templateUrl: './property-list.component.html',
  styleUrls: ['./property-list.component.css'],
})
export class PropertyListComponent implements OnInit {
  id: number;
  accessibility: number;
  property: IProperty;
  allProperties: Array<IProperty>;
  classState: any = {
    displayPicture: true,
  };

  constructor(
    private reqPropertydetails: ActivatedRoute,
    public apiPropertydetails: PropertyService,
    private resPropertydetails: Router
  ) {}

  ngOnInit() {
    this.apiPropertydetails
      .getProperties()
      .toPromise()
      .then(data => {
        this.allProperties = data;
      })
      .catch(err => {
        console.log('List from properties not working', err);
      });
  }

  togglePicture() {
    this.classState.displayPicture = !this.classState.displayPicture;
  }

  getResult() {
    this.apiPropertydetails.getProperty(this.id);
  }
  addProperty() {
    this.apiPropertydetails.saveProperty(this.property);
  }
}
