import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';

import {PropertyRoutingModule} from './property-routing.module';
import {PropertyListComponent} from './property-list.component';
import {PropertyEditComponent} from './property-edit.component';
import {PropertyDetailsComponent} from './property-details.component';

import {PropertyService} from './property.service';
import {PropertyPipe} from './property.pipe';

@NgModule({
  imports: [CommonModule, PropertyRoutingModule, FormsModule],
  declarations: [
    PropertyListComponent,
    PropertyEditComponent,
    PropertyDetailsComponent,
    PropertyPipe,
  ],
  providers: [PropertyService],
})
export class PropertyModule {}
