import {Component, OnInit} from '@angular/core';
import {PropertyService} from './property.service';
import {IProperty} from './property';

@Component({
  selector: 'app-property-edit',
  templateUrl: './property-edit.component.html',
  styleUrls: ['./property-edit.component.css'],
})
export class PropertyEditComponent implements OnInit {
  number = Math.floor(Math.random() * 1001);
  property: IProperty;

  constructor(public apiPropertyDetails: PropertyService) {}

  ngOnInit() {}

  addProperty() {
    console.log(this.property);
    this.apiPropertyDetails.saveProperty(this.property);
  }
}
