import {Pipe, PipeTransform} from '@angular/core';
import {IProperty} from './property';

@Pipe({
  name: 'property',
})
export class PropertyPipe implements PipeTransform {
  transform(value: Array<IProperty>, searchTerm: string): Array<IProperty> {
    if (!value) {
      return [];
    }
    if (!searchTerm) {
      return value;
    }

    searchTerm = searchTerm.toLowerCase();

    const filteredProperty: Array<IProperty> = [];

    value.forEach(oneProperty => {
      const propertyName = oneProperty.name.toLowerCase();
      if (propertyName.includes(searchTerm)) {
        filteredProperty.push(oneProperty);
      }
    });
    return filteredProperty;
  }
}
