import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {HttpModule} from '@angular/http';
import {HttpClientModule} from '@angular/common/http';
import {PropertyService} from './property/property.service';

// Imports for loading & configuring the in-memory web api
import {InMemoryWebApiModule} from 'angular-in-memory-web-api';
import {PropertyData} from './property/property-data';

import {AppRoutingModule} from './app-routing.module';
import {PropertyModule} from './property/property.module';

import {AppComponent} from './app.component';
import {WelcomeComponent} from './welcome/welcome.component';
import {PageNotFoundComponent} from './page-not-found/page-not-found.component';
import {LoginComponent} from './user/login.component';
import {PropertyListComponent} from './property/property-list.component';
import {PropertyDetailsComponent} from './property/property-details.component';
import {PropertyEditComponent} from './property/property-edit.component';
import {PropertyRoutingModule} from './property/property-routing.module';

@NgModule({
  imports: [
    BrowserModule,
    HttpModule,
    FormsModule,
    HttpClientModule,
    InMemoryWebApiModule.forRoot(PropertyData),
    PropertyModule,
    PropertyRoutingModule,
    AppRoutingModule,
  ],
  declarations: [
    AppComponent,
    WelcomeComponent,
    PageNotFoundComponent,
    LoginComponent,
  ],
  providers: [PropertyService],
  bootstrap: [AppComponent],
})
export class AppModule {}
